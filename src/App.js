import './App.css';
import Header from "./components/Header";
import React, { useState, useEffect } from 'react';
import Class from "./components/Class";
import Form from "./components/Form";
import { useSelector } from 'react-redux';

function App() {
    const myClasses = useSelector(store => store.classesList.classes)

    //2

    const classes = [
        {
            id: 1,
            name: "ATP",
            date: "24/5/2021 15:00",
            room: "2001A"
        },
        {
            id: 2,
            name: "Analiza",
            date: "26/5/2021 9:00",
            room: "2251A"
        },
        {
            id: 3,
            name: "SO",
            date: "2/6/2021 15:00",
            room: "21111A"
        },
    ]

    // 3
    const [count, setCount] = useState(0);

    const handleClick = (e) => {
        e.preventDefault();
        setCount(count + 1);
    }

    //4
    const [classList, setClassList] = useState([]);
    const handleAdd = (value) => {
        setClassList([...classList, value]);
    }

    // 5
    useEffect(() => {
        console.log('Page rerendered!');
    })

    useEffect(() => {
        console.log('Element added from form Component!')
    }, [classList])

    return (
        <div className="App">
            {/*1*/}
            <Header />

            {/*2*/}
            {classes.map(clas => (
                <Class key={clas.id} name={clas.name} date={clas.date} room={clas.room} />
            ))}
            <div>
                {/*3*/}
                <p>I have been clicked {count} times</p>
                <button onClick={handleClick}>+1</button>
            </div>
            {/*4*/}
            {classList.map(clas => (
                <Class name={clas.name} date={clas.date} room={clas.room} />
            ))}
            <Form onAdd={handleAdd} />
            {myClasses.map(clas => (
                <Class name={clas.name} date={clas.date} room={clas.room} />
            ))}
        </div>
    );
}

export default App;
