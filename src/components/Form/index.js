import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { addClass } from "../../redux/classes/classes.actions";

const Form = (props) => {
    const dispatch = useDispatch()

    const { onAdd } = props;
    const [formData, setFormData] = useState({
        name: '',
        date: '',
        room: '',
    });

    const handleInputChange = (e) => {
        e.preventDefault();
        setFormData({ ...formData, [e.target.name]: e.target.value })
    }

    const handleClick = () => {
        onAdd(formData);
        dispatch(addClass(formData))
    }

    return (
        <>
            <label>Class:</label>
            <input type={"text"} name={"name"} value={formData.name} onChange={handleInputChange} />
            <label>Date:</label>
            <input type={"text"} name={"date"} value={formData.date} onChange={handleInputChange} />
            <label>Room:</label>
            <input type={"text"} name={"room"} value={formData.room} onChange={handleInputChange} />
            <button onClick={handleClick}>Add</button>

        </>
    )
}

export default Form;
