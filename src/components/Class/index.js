import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { addClass } from '../../redux/classes/classes.actions';
import { selectClassesList } from '../../redux/classes/classes.selector';

const Class = (props) => {
    const store = useSelector(store => store.classesList.classes)
    const dispatch = useDispatch();

    console.log('store', store)

    const { name, date, room } = props;
    return (
        <>
            <p >Class of {name} is going to take place on {date} in the {room} room</p>
        </>
    )
}

export default Class;