import { combineReducers } from 'redux'
// import { persistReducer } from 'redux-presist'
// import storage from 'redux-persist/lib/storage'
import classesReducer from './classes/classes.reducer'

// const persistConfig = {
//     key: 'root',
//     storage,
//     whitelist: ["classes"]
// }

const rootReducer = combineReducers({
    classesList: classesReducer
})

// export default persistReducer(persistConfig, rootReducer)
export default rootReducer;