import { createSelector } from 'reselect'

export const selectClassesList = (state) => state.selectClassesList

export const selectClasses = createSelector(
    [selectClassesList],
    classesList => classesList.classes
)
