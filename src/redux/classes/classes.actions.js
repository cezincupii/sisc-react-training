import { ClassesActionTypes } from './classes.types'

export const setClasses = (classes) => ({
    type: ClassesActionTypes.SET_CLASSES,
    payload: classes
})

export const addClass = (classes) => {
    return {
        type: ClassesActionTypes.ADD_CLASSES,
        payload: classes
    }
}