export const ClassesActionTypes = {
    SET_CLASSES: 'SET_CLASSES',
    ADD_CLASSES: 'ADD_CLASSES'
}