import { ClassesActionTypes } from './classes.types'

const INITAL_STATE = {
    classes: [
        {
            id: 1,
            name: "ATP",
            date: "24/5/2021 15:00",
            room: "2001A"
        },
        {
            id: 2,
            name: "Analiza",
            date: "26/5/2021 9:00",
            room: "2251A"
        },
        {
            id: 3,
            name: "SO",
            date: "2/6/2021 15:00",
            room: "21111A"
        },
    ],
}

const classesReducer = (state = INITAL_STATE, action) => {
    switch (action.type) {
        case ClassesActionTypes.SET_CLASSES:
            return {
                ...state,
                classes: action.payload
            }
        case ClassesActionTypes.ADD_CLASSES:
            return {
                ...state,
                classes: [...state.classes, action.payload]
            }
        default:
            return state
    }
}

export default classesReducer